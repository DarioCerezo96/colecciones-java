package com.cmc.colecciones;

import java.util.HashMap;

import com.cmc.entidades.Contacto;

public class DirectorioMapa extends Directorio {
	HashMap<String,Contacto> contactos;

	public DirectorioMapa() {
		this.contactos = new HashMap<String,Contacto>();;
	}

	@Override
	public void agregarContacto(Contacto contacto) {
		
		if(!contactos.containsKey(contacto.getCedula())){
			contactos.put(contacto.getCedula(),contacto);
			System.out.println("Contacto agregado");
		}else {
			System.out.print("Cedula ya existe");
		}
	}

	@Override
	public Contacto buscarContacto(String cedula) {
		// TODO Auto-generated method stub
		return contactos.get(cedula);
	}

	@Override
	public Contacto eliminarContacto(String cedula) {
		// TODO Auto-generated method stub
		Contacto contacto=contactos.get(cedula);
		
		if(contacto!=null){
			System.out.print("Contacto eliminado............");
			this.contactos.remove(cedula);
			return contacto;
		}
		
		return null;
	}

	@Override
	public void imprimir() {
		// TODO Auto-generated method stub
		for (Contacto contacto : this.contactos.values()) {
			System.out.println("--------------------------"+"\n"
					+contacto.toString());
		}
		
		
	}
}
