package com.cmc.colecciones;

import java.util.ArrayList;

import com.cmc.entidades.Contacto;

public class DirectorioLista extends Directorio {
	ArrayList<Contacto> contactos;

	public DirectorioLista() {
		this.contactos = new ArrayList<Contacto>();
	}

	@Override
	public void agregarContacto(Contacto contacto) {
		
		if(!this.contactos.isEmpty()){
			for (Contacto contact : this.contactos) {
				if (!contact.getCedula().equals(contacto.getCedula())) {
					this.contactos.add(contacto);
					System.out.println("Contacto agregado");
					break;
				} else {
					System.out.print("Cedula ya existe");
				}
			}
		}else{
			this.contactos.add(contacto);
			System.out.println("Contacto agregado");
			
		}
		
	}

	@Override
	public Contacto buscarContacto(String cedula) {
		// TODO Auto-generated method stub
		for (Contacto contacto : this.contactos) {
			if (contacto.getCedula().equals(cedula)) {
				return contacto;
			}
		}
		return null;
	}

	@Override
	public Contacto eliminarContacto(String cedula) {
		// TODO Auto-generated method stub
		for (Contacto contacto : this.contactos) {
			if (contacto.getCedula().equals(cedula)) {
				this.contactos.remove(contacto);
				return contacto;
			}
		}
		return null;
	}

	@Override
	public void imprimir() {
		// TODO Auto-generated method stub
		for (Contacto contacto : this.contactos) {
			System.out.println("--------------------------"+"\n"
					+contacto.toString());
		}
	}

}
