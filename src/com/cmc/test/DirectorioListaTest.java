package com.cmc.test;

import com.cmc.colecciones.DirectorioLista;
import com.cmc.entidades.Contacto;
import com.cmc.entidades.Telefono;

public class DirectorioListaTest {
	public static void main(String[] args) {
		DirectorioLista directorioLista=new DirectorioLista();
		
		Contacto contacto=new Contacto("120259117","Pablo","Cerezo");
		contacto.agregarTelefono(new Telefono("Claro","0989991105"));
		contacto.agregarTelefono(new Telefono("Movistar","0943509894"));
		
		Contacto contacto2=new Contacto("120259417","Tomas","Perezo");
		contacto2.agregarTelefono(new Telefono("Claro","0989991105"));
		
		
		directorioLista.agregarContacto(contacto);
		directorioLista.agregarContacto(contacto2);
		directorioLista.imprimir();
		
		
		directorioLista.eliminarContacto("120259417");
		directorioLista.imprimir();
		
	}
}
