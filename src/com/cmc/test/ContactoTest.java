package com.cmc.test;
import com.cmc.entidades.Contacto;
import com.cmc.entidades.Telefono;

public class ContactoTest {
	public static void main(String[] args) {
		Contacto contacto=new Contacto("120259117","Pablo","Cerezo");
		contacto.agregarTelefono(new Telefono("Claro","0989991105"));
		contacto.agregarTelefono(new Telefono("Movistar","0943509894"));
		System.out.print(contacto.toString());
	}
}
